# == Class: webproxy::packages
#
# Installs / updates all the packages for webproxy
#
# automatically included by webproxy module
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2019 The Board of Trustees of the Leland Stanford Junior
# University
#
class webproxy::packages (
  $install,
  $uninstall,
  $package_type
){

  include stdlib

  Package { provider => $package_type }

  $only_install = difference($install, $uninstall)

  $installing = join($only_install, ' ')
  $uninstalling = join($uninstall, ' ')

  ensure_packages($only_install, { ensure => present })
  ensure_packages($uninstall, { ensure => absent })
}
