# Class: webproxy
# ===========================
#
# configure an Apache + Shibboleth SP + WebAuthLDAP proxy
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2019 The Board of Trustees of the Leland Stanford Junior
# University
#
class webproxy {

  include webproxy::packages

  class { 'shibsp': }

  file { '/efs':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  exec { 'enable proxy':
    command => '/usr/sbin/a2enmod proxy',
  }

  exec { 'enable proxy_http':
    command => '/usr/sbin/a2enmod proxy_http',
  }

  exec { 'enable shib2':
    command => '/usr/sbin/a2enmod shib2',
    require => Package['libapache2-mod-shib2'],
  }

  file { '/etc/apache2/conf-available/webauthldap.conf':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}/webauthldap.conf",
    require => Package['libapache2-mod-webauthldap'],
  }

  exec { 'enable webauthldap':
    command => '/usr/sbin/a2enmod webauthldap',
    require => Package['libapache2-mod-webauthldap'],
  }

  exec { 'enable webauthldap conf':
    command => '/usr/sbin/a2enconf webauthldap',
    require => [
      Package['libapache2-mod-webauthldap'],
      File['/etc/apache2/conf-available/webauthldap.conf']
    ],
  }

}

